<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskCollection;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the Users.
     */
    public function getUsers()
    {
        $users = User::orderBy('id','DESC')->get();
        return response()->json(['message'=>'success','data'=>$users], 200);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return new TaskCollection(Task::with('createdBy')->get());
        $tasks = Task::orderBy('id','DESC')->get();
        for ($i=0; $i <sizeof($tasks); $i++) { 
            $user = User::whereId($tasks[$i]->user_id)->first();
            $tasks[$i]['createdBy'] = $user;
            if(!is_null($tasks[$i]->assigned_to) || !empty($tasks[$i]->assigned_to))
            {
                $tasks[$i]['ass']=$tasks[$i]->assigned_to;
                $assignUser = User::where('id',$tasks[$i]->assigned_to)->first();
                $tasks[$i]['assignTo'] = $assignUser;
            }
        }
        return new TaskCollection($tasks);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTaskRequest $request)
    {
        $data['user_id'] = $request->user_id;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['deadline'] = $request->deadline;
        $data['status'] = "open";
        try {
            DB::begintransaction();
            $saveData = Task::insert($data);
            DB::commit();
            return response()->json([ 'success' => true, 'message' => 'Data store successfully', 'data' => $saveData ], 201);
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            return response()->json([ 'error' => true, 'message' => $message], 500);
        } 
        return response()->json([ 'error' => true, 'message' => 'Something went wrong'], 402);
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
     * Update the specified resource in storage.
     */
    public function assignToUser(Request $request)
    {

        $data['assigned_to'] = (int)$request->assign_to;
        $data['status'] = 'in_progress';
        $id = (int)$request->task_id;
        try {
            DB::begintransaction();
            $task = Task::whereId($id)->first();
            $task->update($data);
            DB::commit();
            return response()->json([ 'success' => true, 'message' => 'Data update successfully', 'data' => $task ], 201);
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            return response()->json([ 'error' => true, 'message' => $message], 500);
        } 
        return response()->json([ 'error' => true, 'message' => 'Something went wrong'], 402);
    }
    public function completeTask(Request $request)
    {
        $data['status'] = 'done';
        $id = (int)$request->task_id;
        try {
            DB::begintransaction();
            $task = Task::whereId($id)->first();
            $task->update($data);
            DB::commit();
            return response()->json([ 'success' => true, 'message' => 'Data update successfully', 'data' => $task ], 201);
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            return response()->json([ 'error' => true, 'message' => $message], 500);
        } 
        return response()->json([ 'error' => true, 'message' => 'Something went wrong'], 402);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        //
    }
}
